function correctInput() {
	let moreThanHundred = prompt('Please input a number greater than 100!');
	return moreThanHundred;
}

let i = 100;

while (i > 0) {
	let userInput = correctInput();

	if (userInput <= i) {
		alert('For real though: put in a number greater than 100!');
	} else {
		alert(`Thanks, ${userInput} is indeed greater than 100!`);
		break;
	}
}
