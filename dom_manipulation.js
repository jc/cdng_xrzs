const container = document.querySelector('#container');

const content = document.createElement('div');
content.classList.add('content');

content.textContent = 'This is the glorious text-content';

container.appendChild(content);

const para = document.createElement('p');
para.style.color = 'red';
para.textContent = "Hey I'm red!";

container.appendChild(para);

const headlineThree = document.createElement('h3');
headlineThree.style.color = 'blue';
headlineThree.textContent = "I'm a blue h3";

container.appendChild(headlineThree);

const divWithBorder = document.createElement('div');
divWithBorder.style.backgroundColor = 'pink';
divWithBorder.style.border = '2px solid black';

container.appendChild(divWithBorder);

const divH = document.createElement('h1');
divH.textContent = "I'm in a div!";

divWithBorder.appendChild(divH);

const pH = document.createElement('p');
pH.textContent = 'ME TOO!';
divWithBorder.appendChild(pH);

function btnClick() {
	alert('Hello World!');
}

// or
// btn.onclick = () => alert("Hello World!");

// const btn = document.querySelector('#btn');
// // btn.onclick = btnClick;

// btn.addEventListener('click', function (e) {
// 	e.target.style.background = 'blue';
// });

const buttons = document.querySelectorAll('button');

buttons.forEach((button) => {
	button.addEventListener('click', () => {
		alert(button.id);
	});
});
